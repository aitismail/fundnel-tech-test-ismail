<?php
/**
 * AuthManager class file
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\components\auth;

use app\components\auth\models\AuthAssignment;
use app\components\auth\models\AuthItem;
use app\components\auth\models\AuthItemChild;
use yii\db\Query;
use yii\rbac\Assignment;
use yii\rbac\Item;
use yii\rbac\Permission;
use yii\rbac\Role;
use Yii;

/**
 * Class AuthManager
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class AuthManager extends \yii\rbac\DbManager
{
    /**
     * @var string
     */
    public $itemTable = AuthItem::TABLE_NAME;

    /**
     * @var string
     */
    public $itemChildTable = AuthItemChild::TABLE_NAME;

    /**
     * @var string
     */
    public $assignmentTable = AuthAssignment::TABLE_NAME;

    /**
     * @var string
     */
    public $ruleTable = 'AuthRule';

    /**
     * @inheritdoc
     * @param integer $userId User ID.
     * @return array
     */
    public function getRolesByUser($userId)
    {
        if (empty($userId)) {
            return [];
        }
        $query = (new Query())->select('b.*')
            ->from(['a' => $this->assignmentTable, 'b' => $this->itemTable])
            ->where('{{a}}.[[itemName]]={{b}}.[[name]]')
            ->andWhere(['a.userId' => $userId]);
        $roles = [];
        foreach ($query->all($this->db) as $row) {
            $roles[$row['name']] = $this->populateItem($row);
        }
        return $roles;
    }

    /**
     * Populates an auth item with the data fetched from database
     * @param mixed $row The data from the auth item table.
     * @return Item the populated auth item instance (either Role or Permission)
     */
    protected function populateItem($row)
    {
        $class = $row['type'] == Item::TYPE_PERMISSION ? Permission::className() : Role::className();
        if (!isset($row['data']) || ($data = @unserialize($row['data'])) === false) {
            $data = null;
        }
        return new $class([
            'name' => $row['name'],
            'type' => $row['type'],
            'description' => $row['description'],
            'ruleName' => $row['ruleName'],
            'data' => $data,
        ]);
    }


    /**
     * @inheritdoc
     * @param integer $userId User ID.
     * @return array
     */
    public function getAssignments($userId)
    {
        if (empty($userId)) {
            return [];
        }
        $query = (new Query())
            ->from($this->assignmentTable)
            ->where(['userId' => $userId]);
        $assignments = [];
        foreach ($query->all($this->db) as $row) {
            $assignments[$row['itemName']] = new Assignment([
                'userId' => $row['userId'],
                'roleName' => $row['itemName'],
            ]);
        }
        return $assignments;
    }

    /**
     * @param mixed   $role   Role.
     * @param integer $userId User ID.
     * @return Assignment
     */
    public function assign($role, $userId)
    {
        $assignment = new Assignment([
            'userId' => $userId,
            'roleName' => $role->name,
            'createdAt' => time(),
        ]);

        $this->db->createCommand()
            ->insert($this->assignmentTable, [
                'userId' => $assignment->userId,
                'itemName' => $assignment->roleName,
                'createdTime' => $assignment->createdAt,
                'createdBy' => Yii::$app->user->isGuest ? null : Yii::$app->user->id
            ])->execute();

        return $assignment;
    }
    
    /**
     * Returns all permissions that are directly assigned to user.
     * @param string|int $userId the user ID (see [[\yii\web\User::id]])
     * @return Permission[] all direct permissions that the user has. The array is indexed by the permission names.
     * @since 2.0.7
     */
    protected function getDirectPermissionsByUser($userId)
    {
        $query = (new Query)->select('b.*')
            ->from(['a' => $this->assignmentTable, 'b' => $this->itemTable])
            ->where('{{a}}.[[itemName]]={{b}}.[[name]]')
            ->andWhere(['a.userId' => (string) $userId])
            ->andWhere(['b.type' => Item::TYPE_PERMISSION]);

        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $this->populateItem($row);
        }
        return $permissions;
    }
    
    /**
     * Returns all permissions that the user inherits from the roles assigned to him.
     * @param string|int $userId the user ID (see [[\yii\web\User::id]])
     * @return Permission[] all inherited permissions that the user has. The array is indexed by the permission names.
     * @since 2.0.7
     */
    protected function getInheritedPermissionsByUser($userId)
    {
        $query = (new Query)->select('itemName')
            ->from($this->assignmentTable)
            ->where(['userId' => (string) $userId]);

        $childrenList = $this->getChildrenList();
        $result = [];
        foreach ($query->column($this->db) as $roleName) {
            $this->getChildrenRecursive($roleName, $childrenList, $result);
        }

        if (empty($result)) {
            return [];
        }

        $query = (new Query)->from($this->itemTable)->where([
            'type' => Item::TYPE_PERMISSION,
            'name' => array_keys($result),
        ]);
        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $this->populateItem($row);
        }
        return $permissions;
    }
}
