<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$userId = Yii::$app->user->id;
        
?>

<div class="list-group">
    
    <a href="<?= Yii::$app->user->isGuest ? 'index' : 'account?id=' . $userId ?>" class="list-group-item">All Categories</a>
    <?php foreach ($categories as $value): ?>
        <?php if (Yii::$app->user->isGuest) : ?>
            <?= Html::a($value->category, ['index', 'categoryId' => $value->id], ['class' => 'list-group-item']) ?>
        <?php else : ?> 
            <?= Html::a($value->category, ['account', 'id' => $userId, 'categoryId' => $value->id], ['class' => 'list-group-item']) ?>
        <?php endif; ?>
    <?php endforeach; ?>
</div>

