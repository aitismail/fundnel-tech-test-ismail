<?php

use yii\db\Migration;

/**
 * Handles the creation for table `ForumGames`.
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.20
 */
class m171020_141123_games_table extends Migration
{
    private $_tableForumTopics = 'ForumTopics';    
    private $_tableForumCategories = 'ForumCategories';
    
    
    /**
     * @inheritdoc
     * @return void
     */
    public function up()
    {
        $this->createForumCategoryTable();
        $this->createForumGameTable();
        $this->addCategoryData();
    }

    /**
     * @inheritdoc
     * @return void
     */
    public function down()
    {
        $this->dropTable($this->_tableForumTopics);
        $this->dropTable($this->_tableForumCategories);
    }
    
    /**
     * Function for create games forum.
     */
    private function createForumGameTable()
    {
        $this->createTable($this->_tableForumTopics, [
            'id' => 'bigint(12) unsigned NOT NULL AUTO_INCREMENT',
            'userId' => 'bigint(12) unsigned NOT NULL',
            'categoryId' => 'bigint(12) unsigned NOT NULL',
            'title' => 'varchar(255) NOT NULL',                
            'slug' => 'varchar(255) NOT NULL',
            'detail' => 'text NULL',
            'isRemoved' => 'tinyint unsigned NOT NULL DEFAULT 0',
            'createdTime' => 'datetime NOT NULL',
            'createdBy' => 'BIGINT UNSIGNED NOT NULL DEFAULT 0',
            'updatedTime' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'updatedBy' => 'BIGINT UNSIGNED NOT NULL DEFAULT 0',
            'PRIMARY KEY (`id`)',
    	], 'ENGINE = InnoDb;');
    }
    
    /**
     * Function for create games categories.
     */
    private function createForumCategoryTable()
    {
        $this->createTable($this->_tableForumCategories, [
            'id' => 'bigint(12) unsigned NOT NULL AUTO_INCREMENT',             
            'category' => 'varchar(80) NOT NULL',
            'isRemoved' => 'tinyint unsigned NOT NULL DEFAULT 0',
            'createdTime' => 'datetime NOT NULL',
            'createdBy' => 'BIGINT UNSIGNED NOT NULL DEFAULT 0',
            'updatedTime' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'updatedBy' => 'BIGINT UNSIGNED NOT NULL DEFAULT 0',
            'PRIMARY KEY (`id`)',
    	], 'ENGINE = InnoDb;');
    }
    
    /**
     * Insert category values.
     */
    private function addCategoryData()
    {
        $categories = $this->categoryList();
        foreach ($categories as $category) {
            $this->insert($this->_tableForumCategories, [
                'category' => $category,
                'createdTime' => date('Y-m-d H:i:s')
            ]);
        }
    }
    
    /**
     * Game category list
     * @return array
     */
    private function categoryList()
    {
        return [
            'Mobile Games',
            'PC Games',
            'Online Games',
            'Web-based Games',
            'Console & Handled Games'
        ];
    }
}
