<?php
/**
 * ForumCategory class file.
 *
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\models;

use app\models\queries\ForumCategoryQuery;
use Yii;

/**
 * This is the model class for table "ForumCategories".
 *
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class ForumCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string
     */
    public static function tableName()
    {
        return 'ForumCategories';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['isRemoved', 'createdBy', 'updatedBy'], 'integer'],
            [['category'], 'string'],
            [['removedTime', 'createdTime', 'updatedTime'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     * @return UserTestimonyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ForumCategoryQuery(get_called_class());
    }
    
    /**
     * @return array
     */
    public static function getCategories()
    {
        return \yii\helpers\ArrayHelper::map(self::find()
                ->asArray()
                ->all(), 'id', 'category');
    }
}
