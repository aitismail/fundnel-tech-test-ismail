<?php
/**
 * Class UserActiveQuery
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\models\queries;

/**
 * UserActiveQuery Description
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class UserActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * @param string $text Text for search.
     * @return $this
     */
    public function filterByUserOrNameOrEmail($text)
    {
        return $this->andFilterWhere(['or',
            ['like','email', $text],
            ['like','fullName', $text]]);
    }

    /**
     * @param string $email Email address in string.
     * @return static
     */
    public function filterByEmail($email)
    {
        return $this->andWhere(['email' => $email]);
    }

    /**
     * @return static
     */
    public function orderByFullName()
    {
        return $this->orderBy(['fullName' => SORT_ASC]);
    }
}
