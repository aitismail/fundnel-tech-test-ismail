<?php
/**
 * ForumPageAsset class file.
 *
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.20 
 */

namespace app\assets;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapPluginAsset;

/**
 * ForumPageAsset handles main assets for forum page.
 *
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.20 
 */
class ForumPageAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/assets/files';

    /**
     * @var array
     */
    public $css = [
        'css/listing.less'
    ];

    /**
     * List of dependencies.
     * @var array
     */
    public $depends = [
        JqueryAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
    ];
}
