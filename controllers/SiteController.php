<?php

namespace app\controllers;

use app\models\ForumCategory;
use app\models\ForumTopic;
use app\models\forms\LoginForm;
use app\models\forms\SignupForm;
use yii\data\ActiveDataProvider;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($categoryId = null)
    {
        $categories = ForumCategory::find()->all();
        
        if ($categoryId !== null) {
            $query = ForumTopic::find()
                ->andWhereCategoryId($categoryId)
                ->andWhereIsNotRemoved();
        } else {
            $query = ForumTopic::find()
                ->andWhereIsNotRemoved();
        }
            
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 5],
            'sort' => [
                'defaultOrder' => [
                    'createdTime' => SORT_DESC
                ]
            ]
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'categories' => $categories
        ]);
    }
    
    /*
     * Detail page.
     */
    public function actionDetail($id)
    {
        $model = ForumTopic::findOne(['id' => $id]);
        
        return $this->render('detail-post', [
            'model' => $model
        ]);
    }

    /**
     * Create new post.
     * @return string
     */
    public function actionCreatePost()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        
        $model = new ForumTopic();
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('createSuccess');
                $userId = Yii::$app->user->id;
                return $this->redirect(['account', 'id' => $userId]);
            }
        }
        return $this->render('new-post', [
            'model' => $model
        ]);
    }
    
    /*
     * Update posted topic.
     */
    public function actionUpdate($id)
    {
        $model = ForumTopic::findOne(['id' => $id]);
        
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('createSuccess');
                $userId = Yii::$app->user->id;
                return $this->redirect(['account', 'id' => $userId]);
            }
        }
        return $this->render('new-post', [
            'model' => $model
        ]);
    }

    /*
     * User Account.
     */
    public function actionAccount($id, $categoryId = null)
    {
        if ($categoryId !== null) {
            $query = ForumTopic::find()
                ->andWhereUserId($id)
                ->andWhereCategoryId($categoryId)
                ->andWhereIsNotRemoved();
        } else {
            $query = ForumTopic::find()
                ->andWhereUserId($id)
                ->andWhereIsNotRemoved();
        }

        $categories = ForumCategory::find()->all();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 5],
            'sort' => [
                'defaultOrder' => [
                    'createdTime' => SORT_DESC
                ]
            ]
        ]);
        return $this->render('account', [
            'dataProvider' => $dataProvider,
            'categories' => $categories
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $userId = Yii::$app->user->id;
            return $this->redirect(['account', 'id' => $userId]);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    
    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post()) && ($user = $model->signup())
            && Yii::$app->getUser()->login($user)) {
            return $this->redirect(['account', 'id' => $user->id]);
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
