<?php
/**
 * Class AuthItem
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\components\auth\models;

/**
 * Class AuthItem
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class AuthItem extends \yii\db\ActiveRecord
{
    const TABLE_NAME = 'AuthItems';

    const ROLE_MEMBER = 'member';
    const ROLE_SUPER_ADMIN = 'super-admin';

    /**
     * @return string
     */
    public static function tableName()
    {
        return static::TABLE_NAME;
    }
}
