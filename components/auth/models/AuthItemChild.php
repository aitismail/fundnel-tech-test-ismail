<?php
/**
 * AuthItemChild class file
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\components\auth\models;

/**
 * Class AuthItemChild
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class AuthItemChild extends \yii\db\ActiveRecord
{
    const TABLE_NAME = 'AuthItemChildren';

    /**
     * @return string
     */
    public static function tableName()
    {
        return static::TABLE_NAME;
    }
}
