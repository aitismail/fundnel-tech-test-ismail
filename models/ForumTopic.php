<?php
/**
 * ForumTopic class file.
 *
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.20 
 */

namespace app\models;

use app\models\User;
use app\models\queries\ForumTopicQuery;
use Yii;

/**
 * This is the model class for table "ForumTopics".
 *
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.20 
 */
class ForumTopic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string
     */
    public static function tableName()
    {
        return 'ForumTopics';
    }
    
    public function behaviors()
    {
      return [
            'comments' => [
                'class' => 'yeesoft\comments\behaviors\CommentsBehavior'
            ]
        ];
    }
    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['categoryId', 'title', 'detail'], 'required'],
            [['userId', 'isRemoved', 'createdBy', 'updatedBy'], 'integer'],
            [['title', 'detail'], 'string'],
            [['removedTime', 'createdTime', 'updatedTime'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     * @return attribute label on topic form
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Category',
        ];
    }

    /**
     * @inheritdoc
     * @return UserTestimonyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ForumTopicQuery(get_called_class());
    }
    
    /**
     * @return queries\ForumCategoryActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ForumCategory::class, ['id' => 'categoryId']);
    }
    
    public function getAuthor()
    {
        $user = User::findOne(['id' => $this->userId]);
        if ($user) {
            return $user->username;
        }
        return null;
    }
}
