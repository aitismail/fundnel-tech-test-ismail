<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>

<section>
    <!-- START row -->
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                <!-- Brand -->
                <div class="text-center">
                    <h3 class=""><?= Yii::t('app', 'Login') ?></h3>
                </div>
                <div class="row">
                    <!-- Login form -->
                    <div class="col-md-12">
                        <?php $form = ActiveForm::begin(['method' => 'post']) ?>
                        <div class="col-md-6 col-sm-6">
                            <?= $form->field($model, 'email')->textInput() ?>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <?= $form->field($model, 'password')->passwordInput() ?>
                        </div>
                        <div class="col-md-12 col-sm-12 pull-right">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary']) ?>
                        </div>
                        <?php ActiveForm::end() ?>
                    </div>
                    <!-- Login form -->
                </div>
                <div class="row ">
                    <div class="col-md-12 pt30">
                        <?= Yii::t('app', 'New User?') ?>
                        <?= Html::a(Yii::t('app', 'Sign up'), ['signup']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ END row -->
</section>
<!--/ END Template Container -->