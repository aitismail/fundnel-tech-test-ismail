<?php

/**
 *
 */

namespace app\models\interfaces;

/**
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.20
 */
interface RemovableModelInterface
{
    /**
     * @event ModelEvent an event that is triggered before marking as trash.
     */
    const EVENT_BEFORE_TRASH = 'beforeTrash';

    /**
     * @event ModelEvent an event that is triggered after marking as trash.
     */
    const EVENT_AFTER_TRASH = 'afterTrash';
    
    /**
     * @event ModelEvent an event that is triggered before marking as trash.
     */
    const EVENT_BEFORE_UNTRASH = 'beforeUntrash';

    /**
     * @event ModelEvent an event that is triggered after marking as trash.
     */
    const EVENT_AFTER_UNTRASH = 'afterUntrash';

    /**
     * Whether the record is flagged as not removed.
     */
    const STATUS_IS_NOT_REMOVED = 0;

    /**
     * Whether the record is flagged as removed.
     */
    const STATUS_IS_REMOVED = 1;
}
