<?php
/* @var $this yii\web\View */

use yeesoft\comments\widgets\Comments;

$this->title = 'Detail Post - ' . ($model->title ? $model->title : '');

?>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><?= $model->title ?></div>
  <div class="panel-body">
    <?= $model->detail ?>
  </div>
</div>
<div class="col-md-12">
    <?= Comments::widget([
        'model' => 'post',
        'model_id' => $model->id
    ]) ?>
</div>


