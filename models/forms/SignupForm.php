<?php
/**
 * SignupForm class file
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\models\forms;

use app\components\auth\models\AuthItem;
use yii\base\Model;
use app\models\User;
use Yii;

/**
 * Class SignupForm
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class SignupForm extends Model
{

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $confirmPassword;
    
    /**
     * @var integer
     */
    public $isActive;
    
    /**
     * @var integer
     */
    public $emailStatus;
    
    /**
     * @var string
     */
    public $activationCode;

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => '\app\models\User',
                'message' => Yii::t('app', 'This email address has already been taken. Please {login} instead', [
                    'login' => Yii::t('app', 'Login')
                ]),
            ],

            ['username', 'trim'],
            ['username', 'required'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['confirmPassword', 'required'],
            ['confirmPassword', 'string'],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->email = $this->email;
        $user->username = $this->username;
        $user->isActive = 1;
        $user->emailStatus = 1;
        $user->activationCode = Yii::$app->security->generateRandomString();
        $user->password = Yii::$app->security->generatePasswordHash($this->password);
        $user->generateAuthKey();

        if ($user->save(false)) {
            $auth = Yii::$app->authManager;
            $auth->assign($auth->getRole(AuthItem::ROLE_MEMBER), $user->id);
            Yii::$app->session->setFlash('signup');
            return $user;
        } else {
            return null;
        }
    }
}
