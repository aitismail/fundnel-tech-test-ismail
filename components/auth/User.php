<?php
/**
 * User class file
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\components\auth;

use app\components\auth\models\AuthItem;
use Yii;

/**
 * Class User
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class User extends \yii\web\User
{
    /**
     * @var array
     */
    private $_roles;

    /**
     * @return array
     */
    public function getRoles()
    {
        if (empty($this->_roles)) {
            $this->_roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
        }
        return $this->_roles;
    }

    /**
     * @return boolean
     */
    public function isSuperAdmin()
    {
        $userRoles = $this->getRoles();
        return isset($userRoles[AuthItem::ROLE_SUPER_ADMIN]);
    }

    /**
     * @return string
     */
    public function getExplicitRole()
    {
        if ($this->isSuperAdmin()) {
            return AuthItem::ROLE_SUPER_ADMIN;
        }

        return AuthItem::ROLE_MEMBER;
    }

    /**
     * @param \yii\web\IdentityInterface $user     Identity object.
     * @param integer                    $duration Login duration.
     * @return boolean
     */
    public function login(\yii\web\IdentityInterface $user, $duration = 0)
    {
        $user->updateCounters(['logins' => 1]);
        $user->lastLoginTime = time();
        $user->lastIP = Yii::$app->request->getUserIP();
        $user->save(false);
        return parent::login($user, $duration);
    }

    /**
     * @return string
     */
    public function getUserTypeLabel()
    {
        if ($this->isGuest) {
            return 'Guest';
        } else {
            return 'Registered User';
        }
    }
}
