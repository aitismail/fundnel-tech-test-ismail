<?php

use yii\db\Migration;

/**
 * Handles the creation for table `Users`.
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class m171022_014318_create_user_table extends Migration
{
    private $_tableUsers = 'Users';
    
    /**
     * @inheritdoc
     * @return void
     */
    public function up()
    {
        $this->createTableUser();
    }

    /**
     * @inheritdoc
     * @return void
     */
    public function down()
    {
        $this->dropTable($this->_tableUsers);
    }
    
    /**
     * Function for create user table.
     */
    private function createTableUser()
    {
        $this->createTable($this->_tableUsers, [
            'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
            'username' => 'varchar(255) NOT NULL',
            'email' => 'VARCHAR(255) NOT NULL',
            'password' => 'VARCHAR(1024) NOT NULL',
            'authKey' => 'VARCHAR(128) NOT NULL',
            'lastIP' => 'VARCHAR(64) NULL',
            'lastActiveTime' => 'INT(10) UNSIGNED NULL',
            'lastPage' => 'TEXT NULL',
            'logins' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
            'isActive' => 'TINYINT(3) UNSIGNED NOT NULL DEFAULT 0',
            'activationCode' => 'VARCHAR(32) NULL',
            'emailStatus' => 'TINYINT(3) UNSIGNED NOT NULL DEFAULT 0',
            'lastLoginTime' => 'INT(11) UNSIGNED NULL',
            'createdTime' => 'INT(11) UNSIGNED NULL',
            'createdBy' => 'INT(11) UNSIGNED NULL',
            'updatedTime' => 'INT(11) UNSIGNED NULL',
            'updatedBy' => 'INT(11) UNSIGNED NULL',
            'removedTime' => 'INT(11) UNSIGNED NULL',
            'removedBy' => 'INT(11) UNSIGNED NULL',
            'isRemoved' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
            'PRIMARY KEY (`id`)',
            'UNIQUE INDEX `email` (`email`)',
            'INDEX `isRemoved` (`isRemoved`)',
            'INDEX `createdTime` (`createdTime`)',
            'INDEX `updatedTime` (`updatedTime`)',
        ], 'ENGINE = InnoDb;');
    }
}
