<?php
/**
 * ForumTopicQuery class file.
 *
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\models\queries;

use app\models\interfaces\RemovableModelInterface;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[ForumTopicQuery]].
 *
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class ForumTopicQuery extends ActiveQuery implements RemovableModelInterface
{
    /**
     * @return static
     */
    public function andWhereUserId($id)
    {
        return $this->andWhere(['userId' => $id]);
    }
    
    /**
     * @return static
     */
    public function andWhereCategoryId($id)
    {
        return $this->andWhere(['categoryId' => $id]);
    }
    
    /**
     * @return static
     */
    public function andWhereIsNotRemoved()
    {
        return $this->andWhere(['isRemoved' => self::STATUS_IS_NOT_REMOVED]);
    }
}
