<?php
/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = 'Account';
?>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?= Html::a(
                'Create Post',
                ['create-post'],
                [
                    'class' => 'btn btn-primary pull-right',
                ]
            ); ?>
        </div>
    </div>
    <div class="row pt30">
        <div class="col-md-3">
            <?= $this->render('_category', [
                'categories' => $categories
            ]) ?>
        </div>
        <div class="col-md-8">
            <?php if (Yii::$app->getSession()->getFlash('createSuccess')): ?>
                <div class="panel-toolbar-wrapper">
                    <div class="alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4 class="semibold"><?= Yii::t('app', 'Create Success') ?></h4>
                        <p class="mb10"><?= Yii::t('app', 'Item was successfully post new topic.') ?></p>
                    </div>
                </div>
            <?php endif; ?>
            <div class="listing-header">Forum Listing</div>
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}\n{pager}\n{summary}",
                    'itemView' => function ($model) {
                        return $this->render('_listItem',['model' => $model]);
                    },
                ])?>
            </div>
    </div>
</section>
