<?php

use yii\db\Migration;

/**
 * Handles the creation for Access control.
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.22
 */
class m171022_032945_create_user_access_control extends Migration
{
    private $_tableAuthItem = 'AuthItems';
    private $_tableAuthItemChild = 'AuthItemChildren';
    private $_tableAuthAssignment = 'AuthAssignments';
    private $_tableAuthRule = 'AuthRules';
    
    /**
     * @inheritdoc
     * @return void
     */
    public function up()
    {
        $this->createTableAuthItem();
        $this->createTableAuthItemChildren();
        $this->createTableAuthAssignment();
        $this->createTableAuthRule();
        
        $this->insertAuthItems();
    }

    /**
     * @inheritdoc
     * @return void
     */
    public function down()
    {
        $this->dropTable($this->_tableAuthItem);
        $this->dropTable($this->_tableAuthItemChild);
        $this->dropTable($this->_tableAuthAssignment);
        $this->dropTable($this->_tableAuthRule);
    }
    
    /**
     * Function for create auth item table.
     */
    private function createTableAuthItem()
    {
        $this->createTable($this->_tableAuthItem, [
            'name' => 'VARCHAR(255) NOT NULL',
            'type' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
            'description' => 'TEXT NULL',
            'ruleName' => 'VARCHAR(255) NULL',
            'data' => 'TEXT NULL',
            'createdTime' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
            'PRIMARY KEY (name)',
            'INDEX `type` (`type`)',
        ], 'ENGINE = InnoDb;');
    }
    
    /**
     * Function for create auth item child table.
     */
    private function createTableAuthItemChildren()
    {
        $this->createTable($this->_tableAuthItemChild, [
            'parent' => 'VARCHAR(255) NOT NULL',
            'child' => 'VARCHAR(255) NOT NULL',
            'PRIMARY KEY (parent, child)',
            'createdTime' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
            'createdBy' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
        ], 'ENGINE = InnoDb;');
    }
    
    /**
     * Function for create auth assignment table.
     */
    private function createTableAuthAssignment()
    {
        $this->createTable($this->_tableAuthAssignment, [
            'itemName' => 'VARCHAR(255) NOT NULL',
            'userId' => 'INT(11) UNSIGNED NOT NULL',
            'createdTime' => 'INT(10) UNSIGNED NOT NULL DEFAULT 0',
            'createdBy' => 'INT(11) UNSIGNED NULL',
            'PRIMARY KEY (itemName, userId)',
        ], 'ENGINE = InnoDb;');
    }
    
    /**
     * Function for create auth rule table.
     */
    private function createTableAuthRule()
    {
        $this->createTable($this->_tableAuthRule, [
            'name' => 'VARCHAR(255) NOT NULL',
            'data' => 'TEXT NULL',
            'createdTime' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
            'PRIMARY KEY (name)',
        ], 'ENGINE = InnoDb;');
    }
    
    private function insertAuthItems()
    {
        $this->insert($this->_tableAuthItem,
        [
            'name' => 'super-admin',
            'type' => 1,
            'createdTime' => time(),
        ]);
        
        $this->insert($this->_tableAuthItem,
        [
            'name' => 'member',
            'type' => 1,
            'createdTime' => time(),
        ]);
    }
}
