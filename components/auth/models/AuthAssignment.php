<?php
/**
 * AuthAssignment class file
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\components\auth\models;

/**
 * Class AuthAssignment
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class AuthAssignment extends \yii\db\ActiveRecord
{
    const TABLE_NAME = 'AuthAssignments';

    /**
     * @return string
     */
    public static function tableName()
    {
        return static::TABLE_NAME;
    }
}
