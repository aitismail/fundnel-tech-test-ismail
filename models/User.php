<?php
/**
 * User class file
 *
 * @package app\models
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\models;

use app\components\auth\models\AuthAssignment;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $activationCode
 * @property boolean $isActive
 * @property string $email
 * @property string $authKey
 * @property string $password
 * @property string $companyName
 * @property string $telephone
 * @property string $fullName
 * @property boolean $emailStatus
 * @property string $linkedinId
 * @property string $facebookId
 * @property boolean $isMarketingConsultant
 * @property string $photoPath
 * @property integer $marketingConsultantTitleId
 * @property string $lastIP
 * @property integer $lastActiveTime
 * @property integer $lastLoginTime
 * @property boolean $isShownAsContact
 *
 * @package app\models
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class User extends ActiveRecord implements IdentityInterface
{   
    const EVENT_BEFORE_SAVE = 'beforeSave';

    /**
     * @inheritdoc
     * @return string
     */
    public static function tableName()
    {
        return 'Users';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdTime',
                'updatedAtAttribute' => 'updatedTime',
            ],
            [
                'class' => \yii\behaviors\BlameableBehavior::class,
                'createdByAttribute' => 'createdBy',
                'updatedByAttribute' => 'updatedBy',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['fullName', 'telephone', 'email'], 'required'],
            [['activationCode'], 'safe'],
        ]);
    }
    
    /**
     * Set user activation code event after the model is save.
     * @param string $insert Attribute.
     * @return string
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        $this->generateRandomActivationCode();
        return true;
    }

    /**
     * @param integer $id User ID.
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'isActive' => 1]);
    }

    /**
     * @inheritdoc
     *
     * @param string $token User access token.
     * @param mixed  $type  Type.
     *
     * @throws NotSupportedException Thrown because method not yet implemented.
     * @return mixed
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $type; //unused
        return static::findOne(['email' => $token, 'isActive' => 1]);
    }

    /**
     * Finds user by username
     *
     * @param string $username Username.
     * @return static|null
     */
    public static function findByUsernameOrEmail($username)
    {
        return static::findOne(['email' => $username, 'isActive' => 1]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token Password reset token.
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'activationCode' => $token,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token Password reset token.
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     * @return integer
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @param string $authKey Auth key.
     * @return boolean
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password Password to validate.
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }
    
    /**
     * Generate random string for activation code.
     * @return void
     */
    public function generateRandomActivationCode()
    {
        $this->activationCode = Yii::$app->security->generateRandomString();
    }
    
    /**
     * Hash user password.
     * @param string $password Input password.
     * @return void
     */
    public function generatePasswordHash($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     * @return void
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     * @return void
     */
    public function generatePasswordResetToken()
    {
        $this->activationCode = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     * @return void
     */
    public function removePasswordResetToken()
    {
        $this->activationCode = null;
    }

    /**
     * @inheritdoc
     * @return queries\UserActiveQuery
     */
    public static function find()
    {
        return new queries\UserActiveQuery(get_called_class());
    }
        
    /**
     * @return \common\components\auth\models\AuthAssignment
     */
    public function getAuthAssignment()
    {
        return $this->hasMany(AuthAssignment::class, ['userId' => 'id']);
    }
}
