<?php
/**
 * ForumCategoryQuery class file.
 *
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[ForumCategoryQuery]].
 *
 * @author Ismail <is.tmdg86@gmail.com>
 * @since 2017.10.21
 */
class ForumCategoryQuery extends \yii\db\ActiveQuery
{

}
