<?php
/* @var $this yii\web\View */

use app\models\ForumCategory;
use dosamigos\ckeditor\CKEditor;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'New Post';

$model->userId = Yii::$app->user->id;
?>

<section class="content">
    <div class="row">
        <div class="col-md-12 content-right pl0">
            <h2 class="title-page">
                Post New Topic
            </h2>
            <div class="col-md-7 enquire-form-item">
                <?php $form = ActiveForm::begin(); ?>
                <div style="display: none">
                    <?= $form->field($model, 'userId')->hiddenInput()->label(false); ?>
                </div>
                <div class="form-group ">
                    <?= $form->field($model, 'title')->textInput(['placeholder' => Yii::t('app', 'Topic title'), 'class' => 'form-control'])->label(null, ['class' => 'control-label']); ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'categoryId')->dropDownList(ForumCategory::getCategories(), [
                        'prompt' => 'Choose Category',
                    ]);?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'detail')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'preset' => 'basic'
                    ]) ?>
                </div>
                <div class="form-group right">
                    <?= Html::submitButton(Yii::t('app', 'Post Now'), ['class' => 'btn btn-primary']); ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>


