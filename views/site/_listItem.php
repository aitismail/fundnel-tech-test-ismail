<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="list-item">
    <div class="col-md-10">
        <?= Html::a($model->title,
            ['detail', 'id' => $model->id])
        ?>
    </div>
    <div class="col-md-2">
        <?php if (!Yii::$app->user->isGuest) :?>
        <?= Html::a('edit', 
            ['update', 'id' => $model->id],
            ['class' => 'btn btn-default btn-xs pull-right']) 
        ?>
        <?php endif; ?>
    </div>
    <div class="list-item-author">
        <ul>
            <li><?= Html::a('Category : ' . $model->category->category)?></li>
            <li><?= Html::a('Publisher : ' . $model->getAuthor())?></li>
        </ul>
    </div>
</div>
