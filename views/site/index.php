<?php
/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = 'Game Forum';
?>

<section class="content">
    <div class="row pt30">
        <div class="col-md-3">
            <?= $this->render('_category', [
                'categories' => $categories
            ]) ?>
        </div>
        <div class="col-md-8">
            <div class="listing-header">Forum Listing</div>
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}\n{pager}\n{summary}",
                    'itemView' => function ($model) {
                        return $this->render('_listItem',['model' => $model]);
                    },
                ])?>
            </div>
    </div>
</section>
